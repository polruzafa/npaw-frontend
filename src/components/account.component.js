import React from 'react'
import Badge from './badge.component'

export default props => {
  return (
    <tr onClick={props.onClick} className='custom-row'>
      <td className='mw-300'>
        { props.account._id }
      </td>
      <td className='mw-300'>
        { props.account.name }
      </td>
      <td>
        { props.account.permissions.map(permission => <Badge key={permission._id} permission={permission} />) }
      </td>
      <td className='mw-50'>
        <button className='btn btn-outline-info mr-2' onClick={props.editClick}>
          Edit
        </button>
        <button className='btn btn-outline-danger' onClick={props.deleteClick}>
          Delete
        </button>
      </td>
    </tr>
  )
}
