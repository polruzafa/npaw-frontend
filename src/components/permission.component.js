import React from 'react'

export default props => {
  return (
    <tr>
      <td className='mw-300'>
        { props.permission._id }
      </td>
      <td className='mw-300'>
        { props.permission.name }
      </td>
      <td>
        { props.permission.description }
      </td>
      <td className='mw-50'>
        <button className='btn btn-outline-danger' onClick={props.deleteClick}>
          Delete
        </button>
      </td>
    </tr>
  )
}
