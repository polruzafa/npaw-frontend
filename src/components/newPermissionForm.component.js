import React from 'react'
import { Field, reduxForm } from 'redux-form'

let NewPermissionForm = props => {
  const { handleSubmit } = props

  return (
    <form onSubmit={handleSubmit}>
      <div className='form-group'>
        <label htmlFor='name'>Permission name</label>
        <Field name='name' component='input' type='text' required className='form-control' />
        <label htmlFor='description' className='mt-3'>Description</label>
        <Field name='description' component='textarea' required className='form-control' />
        <button className='btn btn-secondary mt-3' type='submit'>
          Add new permission
        </button>
      </div>
    </form>
  )
}

NewPermissionForm = reduxForm({
  form: 'newPermission'
})(NewPermissionForm)

export default NewPermissionForm
