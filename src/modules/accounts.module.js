import axios from 'axios'
import initialState from './state'

// GET
export const FETCH_ACCOUNTS_BEGIN = 'accounts/FETCH_ACCOUNTS_BEGIN'
export const FETCH_ACCOUNTS_SUCCESS = 'accounts/FETCH_ACCOUNTS_SUCCESS'
export const FETCH_ACCOUNTS_FAILURE = 'accounts/FETCH_ACCOUNTS_FAILURE'
// POST
export const POST_ACCOUNTS_BEGIN = 'accounts/POST_ACCOUNTS_BEGIN'
export const POST_ACCOUNTS_SUCCESS = 'accounts/POST_ACCOUNTS_SUCCESS'
export const POST_ACCOUNTS_FAILURE = 'accounts/POST_ACCOUNTS_FAILURE'
// PUT
export const PUT_ACCOUNTS_BEGIN = 'accounts/PUT_ACCOUNTS_BEGIN'
export const PUT_ACCOUNTS_SUCCESS = 'accounts/PUT_ACCOUNTS_SUCCESS'
export const PUT_ACCOUNTS_FAILURE = 'accounts/PUT_ACCOUNTS_FAILURE'
// DEL
export const DEL_ACCOUNTS_BEGIN = 'accounts/DEL_ACCOUNTS_BEGIN'
export const DEL_ACCOUNTS_SUCCESS = 'accounts/DEL_ACCOUNTS_SUCCESS'
export const DEL_ACCOUNTS_FAILURE = 'accounts/DEL_ACCOUNTS_FAILURE'
// OTHERS
export const CLEAR_ACCOUNT_CREATED = 'accounts/CLEAR_ACCOUNT_CREATED'

// Reducers
export default (state = initialState, action) => {
  switch (action.type) {
    case FETCH_ACCOUNTS_BEGIN:
      return {
        ...state,
        loading: true,
        error: null
      }
    case FETCH_ACCOUNTS_SUCCESS:
      return {
        ...state,
        loading: false,
        list: action.payload.accounts
      }
    case FETCH_ACCOUNTS_FAILURE:
      return {
        ...state,
        loading: false,
        error: action.payload.error,
        list: []
      }
    case POST_ACCOUNTS_BEGIN:
      return {
        ...state,
        loading: true,
        error: null
      }
    case POST_ACCOUNTS_SUCCESS:
      return {
        ...state,
        loading: false,
        accountCreated: action.payload.account
      }
    case POST_ACCOUNTS_FAILURE:
      return {
        ...state,
        loading: false,
        error: action.payload.error
      }
    case PUT_ACCOUNTS_BEGIN:
      return {
        ...state,
        loading: true,
        error: null
      }
    case PUT_ACCOUNTS_SUCCESS:
      return {
        ...state,
        loading: false,
        accountCreated: action.payload.account
      }
    case PUT_ACCOUNTS_FAILURE:
      return {
        ...state,
        loading: false,
        error: action.payload.error
      }
    case DEL_ACCOUNTS_BEGIN:
      return {
        ...state,
        loading: true,
        error: null
      }
    case DEL_ACCOUNTS_SUCCESS:
      return {
        ...state,
        loading: false,
        list: state.list.filter(item => item._id !== action.payload.id)
      }
    case DEL_ACCOUNTS_FAILURE:
      return {
        ...state,
        loading: false,
        error: action.payload.error
      }
    case CLEAR_ACCOUNT_CREATED:
      return {
        ...state,
        loading: false,
        error: null,
        accountCreated: undefined
      }
    default:
      return state
  }
}

// Action Creators
export const fetchAccounts = id => {
  return dispatch => {
    dispatch({
      type: FETCH_ACCOUNTS_BEGIN
    })
    if (id) {
      return axios.get(`http://localhost:8088/api/accounts/${id}`).then(
        response => dispatch({ type: FETCH_ACCOUNTS_SUCCESS, payload: { accounts: response.data } }),
        error => dispatch({ type: FETCH_ACCOUNTS_FAILURE, payload: { error } })
      )
    }
    return axios.get('http://localhost:8088/api/accounts').then(
      response => dispatch({ type: FETCH_ACCOUNTS_SUCCESS, payload: { accounts: response.data } }),
      error => dispatch({ type: FETCH_ACCOUNTS_FAILURE, payload: { error } })
    )
  }
}
export const postAccount = account => {
  return dispatch => {
    dispatch({
      type: POST_ACCOUNTS_BEGIN
    })

    return axios.post('http://localhost:8088/api/accounts', {account}, { headers: {'Content-Type': 'application/json'} }).then(
      response => dispatch({ type: POST_ACCOUNTS_SUCCESS, payload: { account: response.data } }),
      error => dispatch({ type: POST_ACCOUNTS_FAILURE, payload: { error } })
    )
  }
}
export const putAccount = (id, account) => {
  return dispatch => {
    dispatch({
      type: PUT_ACCOUNTS_BEGIN
    })

    return axios.put(`http://localhost:8088/api/accounts/${id}`, {account}, { headers: {'Content-Type': 'application/json'} }).then(
      response => dispatch({ type: PUT_ACCOUNTS_SUCCESS, payload: { account: response.data } }),
      error => dispatch({ type: PUT_ACCOUNTS_FAILURE, payload: { error } })
    )
  }
}
export const delAccount = account => {
  return dispatch => {
    dispatch({
      type: DEL_ACCOUNTS_BEGIN
    })

    return axios.delete(`http://localhost:8088/api/accounts/${account._id}`).then(
      response => dispatch({ type: DEL_ACCOUNTS_SUCCESS, payload: { id: response.data } }),
      error => dispatch({ type: DEL_ACCOUNTS_FAILURE, payload: { error } })
    )
  }
}

export const clearAccountCreated = () => {
  return dispatch => dispatch({type: CLEAR_ACCOUNT_CREATED})
}
