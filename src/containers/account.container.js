import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import { fetchPermissions } from '../modules/permissions.module'
import { fetchAccounts, postAccount, putAccount, clearAccountCreated } from '../modules/accounts.module'

import NewAccountForm from '../components/newAccountForm.component'

class AddAccount extends Component {
  constructor (props) {
    super(props)
    const { match } = props
    this.messages = {
      header: 'Add a new account',
      button: 'Add new account'
    }
    if (match.params.id !== 'add') {
      this.messages.header = 'Edit account'
      this.messages.button = 'Save changes'
      this.props.fetchAccounts(match.params.id)
      this.submit = values => this.props.putAccount(match.params.id, values)
    } else {
      this.submit = values => this.props.postAccount(values)
    }
  }

  componentWillReceiveProps (props) {
    if (props.accounts.accountCreated) {
      this.props.clearAccountCreated()
      this.props.history.push('/accounts')
    }
  }

  componentDidMount () {
    this.props.fetchPermissions()
    console.log('[AddAccount] I did mount')
  }

  render () {
    return (
      <div className='container'>
        <div className='row-justify-content-md-center mt-4'>
          <div className='col-md-4'>
            <h2>{this.messages.header}</h2>
            <NewAccountForm onSubmit={this.submit} permissions={this.props.permissions.list} account={this.props.accounts.list} onButton={this.messages.button} />
          </div>
        </div>
      </div>
    )
  }
}

// Map redux state to props
function mapStateToProps (state) {
  return {
    permissions: state.permissions,
    accounts: state.accounts
  }
}
// Map actions to props
const mapDispatchToProps = dispatch => bindActionCreators({
  fetchPermissions,
  fetchAccounts,
  postAccount,
  putAccount,
  clearAccountCreated
}, dispatch)
// Promote Accounts from component to container
export default connect(mapStateToProps, mapDispatchToProps)(AddAccount)
