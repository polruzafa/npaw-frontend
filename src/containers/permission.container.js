import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import { postPermission, clearPermissionCreated } from '../modules/permissions.module'

import NewPermissionForm from '../components/newPermissionForm.component'

class AddPermission extends Component {
  constructor (props) {
    super(props)
    this.submit = values => this.props.postPermission(values)
  }

  componentWillReceiveProps (props) {
    if (props.permissions.permissionCreated) {
      this.props.clearPermissionCreated()
      this.props.history.push('/permissions')
    }
  }

  componentDidMount () {
    console.log('[AddPermission] I did mount')
  }

  render () {
    return (
      <div className='container'>
        <div className='row-justify-content-md-center mt-4'>
          <div className='col-md-4'>
            <h2>Add a new permission</h2>
            <NewPermissionForm onSubmit={this.submit} permissions={this.props.permissions.list} />
          </div>
        </div>
      </div>
    )
  }
}

// Map redux state to props
function mapStateToProps (state) {
  return {
    permissions: state.permissions
  }
}
// Map actions to props
const mapDispatchToProps = dispatch => bindActionCreators({
  postPermission,
  clearPermissionCreated
}, dispatch)
// Promote Permissions from component to container
export default connect(mapStateToProps, mapDispatchToProps)(AddPermission)
